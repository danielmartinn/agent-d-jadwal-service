package com.agentd.jadwalservice.controller;

import com.agentd.jadwalservice.core.Jadwal;
import com.agentd.jadwalservice.core.UserAgentD;
import com.agentd.jadwalservice.service.JadwalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/jadwal")
public class JadwalServiceController {

    @Autowired
    private JadwalService service;

    @GetMapping("/find-user/{id}")
    public ResponseEntity<UserAgentD> findPenggunaById(@PathVariable String id){
        return new ResponseEntity<UserAgentD>(service.findUser(id), HttpStatus.OK);
    }

    @PostMapping("/register-user/{id}")
    public ResponseEntity registerPenggunaById(@PathVariable String id){
        service.registerUser(new UserAgentD(id));
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/register-jadwal/{id}/{name}/{day}/{timeStart}/{timeEnd}")
    public ResponseEntity registerJadwalUntukPengguna(
            @PathVariable String id,
            @PathVariable String name,
            @PathVariable String day,
            @PathVariable String timeStart,
            @PathVariable String timeEnd

    ){
        service.registerJadwal(id,name,day,timeStart,timeEnd);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/lihat-jadwal/{id}")
    public ResponseEntity<List<Jadwal>> findAllJadwal(@PathVariable String id){
        return new ResponseEntity<List<Jadwal>>(service.findAllJadwalOfUser(id),HttpStatus.OK);
    }
}
