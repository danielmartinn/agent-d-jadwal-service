package com.agentd.jadwalservice.core;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class UserAgentD {
    String id;
    ArrayList<Jadwal> listJadwal;
    static LogManager lgmngr = LogManager.getLogManager();
    static Logger log = lgmngr.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public UserAgentD(String userID){
        id = userID;
        this.listJadwal = new ArrayList<>();
    }

    void addJadwal(Jadwal jadwal){
        listJadwal.add(jadwal);
    }

    public String getId(){
        return this.id;
    }

    public void tambahJadwal(String name, String day, String timeStart, String timeEnd) {
        try {
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
            Date timeStartDate = timeFormat.parse(timeStart);
            Date timeEndDate = timeFormat.parse(timeEnd);
            Jadwal jadwal = new Jadwal(name, day, timeStartDate, timeEndDate);
            this.addJadwal(jadwal);
        } catch (ParseException e) {
            log.log(Level.INFO, "Error while parsing the date");
        }
    }

    public List<Jadwal> lihatJadwal(){
        return this.listJadwal;
    }
}
