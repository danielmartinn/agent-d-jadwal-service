package com.agentd.jadwalservice.service;


import com.agentd.jadwalservice.core.Jadwal;
import com.agentd.jadwalservice.core.UserAgentD;
import com.agentd.jadwalservice.repository.UserRepository;
import org.springframework.stereotype.Service;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JadwalServiceImpl implements JadwalService {

    private UserRepository repository = new UserRepository();

    @Override
    public UserAgentD findUser(String id) {
        return repository.getUserById(id);
    }

    @Override
    public void registerUser(UserAgentD pengguna) {
        repository.addUser(pengguna);
    }

    @Override
    public void registerJadwal(String id,String nama, String day, String timeStart, String timeEnd) {
        UserAgentD user = this.findUser(id);
        user.tambahJadwal(nama,day, timeStart, timeEnd);
    }

    public UserRepository getRepository(){
        return this.repository;
    }

    @Override
    public List<Jadwal> findAllJadwalOfUser(String id) {
        UserAgentD user = this.findUser(id);
        return user.lihatJadwal();
    }

}
