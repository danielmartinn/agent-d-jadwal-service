package com.agentd.jadwalservice.service;

import com.agentd.jadwalservice.core.Jadwal;
import com.agentd.jadwalservice.core.UserAgentD;

import java.util.List;

public interface JadwalService{
    public UserAgentD findUser(String id);
    public void registerUser(UserAgentD pengguna);
    public void registerJadwal(String id,String nama, String day, String timeStart, String timeEnd);
    public List<Jadwal> findAllJadwalOfUser(String id);

}
