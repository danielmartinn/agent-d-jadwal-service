package com.agentd.jadwalservice.controller;

import com.agentd.jadwalservice.core.Jadwal;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = JadwalServiceController.class)
public class JadwalServiceControllerTests {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private Jadwal jadwal;

    @Test
    public void whenRegisterJadwalURIIsAccessedItShouldHandledByRegisterPenggunaById() throws Exception {
        mockMvc.perform(post("/jadwal/register-user/123abc")).andExpect(status().isOk()).andExpect(handler().methodName("registerPenggunaById"));
    }

    @Test
    public void whenFindJadwalURIIsAccessedItShouldHandledByFindPenggunaById() throws Exception {
        mockMvc.perform(get("/jadwal/find-user/123abc")).andExpect(status().isOk()).andExpect(handler().methodName("findPenggunaById"));
    }

    @Test
    public void testMethodregisterJadwal() throws Exception {
        mockMvc.perform(post("/jadwal/register-jadwal/123abc/Jadwal Baru/Monday/13:00:00/15:00:00")).andExpect(status().isOk()).andExpect(handler().methodName("registerJadwalUntukPengguna"));
    }

    @Test
    public void testMethodLihatJadwal() throws Exception {
        mockMvc.perform(get("/jadwal/lihat-jadwal/123abc")).andExpect(status().isOk()).andExpect(handler().methodName("findAllJadwal"));
    }
}
