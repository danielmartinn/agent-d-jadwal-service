package com.agentd.jadwalservice.service;

import com.agentd.jadwalservice.core.Jadwal;
import com.agentd.jadwalservice.core.UserAgentD;
import org.apache.catalina.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class JadwalServiceImplTests {
    JadwalServiceImpl service;
    UserAgentD user;
    Jadwal jadwal;
    SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
    @BeforeEach
    public void setUp(){
        try {
            service = new JadwalServiceImpl();
            user = new UserAgentD("123abc");
            Date startTime = timeFormat.parse("13:00:00");
            Date endTime = timeFormat.parse("15:00:00");
            jadwal = new Jadwal("Jadwal Baru", "Monday", startTime, endTime);
        }catch (ParseException e){
            e.printStackTrace();
        }
    }

    @Test
    public void testMethodRegisterUser(){
        service.registerUser(user);
        assertEquals(1,service.getRepository().getRepo().size());
    }

    @Test
    public void testMethodFindUser(){
        service.registerUser(user);
        assertEquals(user,service.findUser("123abc"));
    }

    @Test
    public void testMethodRegisterJadwal(){
        service.registerUser(user);
        service.registerJadwal(user.getId(),"Jadwal Baru","Monday","13:00:00", "15:00:00");
        assertEquals(1,service.findUser(user.getId()).lihatJadwal().size());
    }

    @Test
    public void testMethodFindAllTugasIndividu(){
        service.registerUser(user);
        service.registerJadwal(user.getId(),"Jadwal Baru","Monday","13:00:00", "15:00:00");
        assertEquals(1,service.findAllJadwalOfUser("123abc").size());
    }
}
