package com.agentd.jadwalservice.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JadwalTests {
    private Jadwal jadwal;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

    @BeforeEach
    public void setUp() {
        try {
            Date date = dateFormat.parse("01/01/2020");
            Date startTime = timeFormat.parse("13:00:00");
            Date endTime = timeFormat.parse("15:00:00");
            jadwal = new Jadwal("Jadwal Baru", "Monday", startTime, endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testMethodGetName() {
        assertEquals("Jadwal Baru", jadwal.getName());
    }

    @Test
    public void testMethodSetName() {
        jadwal.setName("Jadwal Baru 2");
        assertEquals("Jadwal Baru 2", jadwal.getName());
    }

    @Test
    public void testMethodGetDay() {
        String day = jadwal.getDay();
        assertEquals("Monday", day);
    }

    @Test
    public void testMethodGetTimeStart() {
        Date timeStart = jadwal.getTimeStart();
        String strTime = timeFormat.format(timeStart);
        assertEquals("13:00:00", strTime);
    }

    @Test
    public void testMethodGetTimeEnd() {
        Date timeEnd = jadwal.getTimeEnd();
        String strTime = timeFormat.format(timeEnd);
        assertEquals("15:00:00", strTime);
    }
}
