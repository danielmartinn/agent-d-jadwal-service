package com.agentd.jadwalservice.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserAgentDTests {
    UserAgentD user;

    @BeforeEach
    public void setUp(){
        user = new UserAgentD("123abc");
    }

    @Test
    public void testMethodAddJadwal() {
        try {
            assertEquals(0, user.lihatJadwal().size());
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
            Date timeStart = timeFormat.parse("12:00:00");
            Date timeEnd = timeFormat.parse("14:00:00");
            Jadwal jadwal1 = new Jadwal("Jadwal 1", "Monday", timeStart, timeEnd);
            user.addJadwal(jadwal1);
            assertEquals(1,user.lihatJadwal().size());
        }catch (ParseException e){
            e.printStackTrace();
        }
    }

    @Test
    public void testMethodGetJadwal(){
        try {
            assertEquals(0, user.lihatJadwal().size());
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
            Date date = dateFormat.parse("05/04/2020");
            Date timeStart = timeFormat.parse("13:00:00");
            Date timeEnd = timeFormat.parse("14:00:00");
            Jadwal jadwal2 = new Jadwal("Jadwal 2", "Tuesday", timeStart, timeEnd);
            user.addJadwal(jadwal2);
            assertEquals("Jadwal 2", user.lihatJadwal().get(0).getName());
        }catch (ParseException e){
            e.printStackTrace();
        }
    }

    @Test
    public void testMethodTambahTugasIndividu2(){
        user.tambahJadwal("Jadwal Baru", "Monday","13:00:00", "ab:00:00");
        assertEquals(0,user.lihatJadwal().size());
    }
}
